//
//  UIViewControllerExtension.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentDetail(_ viewControllerToPresent: UIViewController, withTransition type: String) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = convertToCATransitionType(type)
        self.view.window?.layer.add(transition, forKey: kCATransition)
    
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    func presentSecondaryDetail(_ viewControllerToPresent: UIViewController, withTransition type: String) {
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = convertToCATransitionType(type)
        
        guard let presentedViewController = presentedViewController else { return }
        
        presentedViewController.dismiss(animated: false) {
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.present(viewControllerToPresent, animated: false, completion: nil)
        }
    }
    
    func dismissDetail(withTransition type: String) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = convertToCATransitionType(type)
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToCATransitionType(_ input: String) -> CATransitionType {
	return CATransitionType(rawValue: input)
}
