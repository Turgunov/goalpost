//
//  CreateGoalViewController.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class CreateGoalViewController: UIViewController, UITextViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var goalTextView: UITextView! { didSet { goalTextView.delegate = self } }
    @IBOutlet weak var shortTermButton: UIButton!
    @IBOutlet weak var longTermButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: - Private Variables
    private var goalType: GoalType = .shortTerm
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.bindToKeyboard()
        shortTermButton.setSelectedColor()
        longTermButton.setDeselectedColor()
        addTapGestureToEndEditing()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismissDetail(withTransition: convertFromCATransitionType(CATransitionType.fade))
    }
    
    @IBAction func shortTermButtonPressed(_ sender: UIButton) {
        goalType = .shortTerm
        shortTermButton.setSelectedColor()
        longTermButton.setDeselectedColor()
    }
    @IBAction func longTermButtonPressed(_ sender: UIButton) {
        goalType = .longTerm
        longTermButton.setSelectedColor()
        shortTermButton.setDeselectedColor()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if goalTextView.text != "" && goalTextView.text != "What is your goal ?" {
            guard let finishGoalVC = storyboard?.instantiateViewController(withIdentifier: StoryboardViewControllerIDs.finishGoalVC) as? FinishGoalViewController else { return }
            finishGoalVC.initData(description: goalTextView.text!, type: goalType)
            presentingViewController?.presentSecondaryDetail(finishGoalVC, withTransition: convertFromCATransitionType(CATransitionType.fade))
        }
    }

    private func addTapGestureToEndEditing() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(_:)))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func tapGestureHandler(_ gesture: UITapGestureRecognizer) {
        goalTextView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        goalTextView.text = ""
        goalTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATransitionType(_ input: CATransitionType) -> String {
	return input.rawValue
}
