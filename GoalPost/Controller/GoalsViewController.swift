//
//  GoalsViewController.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class GoalsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.isHidden = false
        }
    }
    @IBOutlet weak var undoRemoveView: UIView! { didSet { addSwipe() } }
    
    private var goals: [Goal] = []
    private var lastDeletedGoal: GoalData?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoreDataObjects()
        tableView.reloadData()
    }
    
    private func fetchCoreDataObjects() {
        fetch { success in
            if success {
                if goals.count >= 1 {
                    tableView.isHidden = false
                }else {
                    tableView.isHidden = true
                }
            }
        }
    }
    
    @IBAction func addGoalButtonPressed(_ sender: UIButton) {
        guard let createGoalVC = storyboard?.instantiateViewController(withIdentifier: StoryboardViewControllerIDs.createGoalVC) else { return }
        presentDetail(createGoalVC, withTransition: convertFromCATransitionType(CATransitionType.fade))
    }
    
    @IBAction func undoRemoveButtonPressed(_ sender: UIButton) {
        recoverDeletedGoal { success in
            if success {
                print("Undo success!")
                self.fetchCoreDataObjects()
                
                if self.goals.count >= 1 {
                    self.tableView.isHidden = false
                } else {
                    self.tableView.isHidden = true
                }
                self.tableView.reloadData()
            } else {
                print("Undo failed!")
            }
        }
    }
}

extension GoalsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.goalCell, for: indexPath) as? GoalCell else { return UITableViewCell() }
        let goal = goals[indexPath.row]
        cell.configureCell(goal: goal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
            
            self.removeGoal(atIndexPath: indexPath)
            self.fetchCoreDataObjects()
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            
            self.undoRemoveView.isHidden = false
        }
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        
        let addAction = UITableViewRowAction(style: .normal, title: "+1") { (rowAction, indexPath) in
            self.setProgressForGoal(atIndexPath: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        addAction.backgroundColor = #colorLiteral(red: 0.9385011792, green: 0.7164435983, blue: 0.3331357837, alpha: 1)
        
        return [deleteAction, addAction]
    }
    
    func removeGoal(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        // Clone goal data and stach it for later
        let undoableGoal = GoalData(fromGoalEntity: self.goals[indexPath.row])
        self.lastDeletedGoal = undoableGoal
        
        managedContext.delete(goals[indexPath.row])
        
        do {
            try managedContext.save()
            print("Successfully removed from CoreData")
        }catch {
            debugPrint("Could not remove: \(error.localizedDescription)")
        }
    }
    
    func recoverDeletedGoal(completion: @escaping CompletionHandler)
    {
        guard let goalToRecover = self.lastDeletedGoal else {
            debugPrint("Could not undo deleted goal: There was no deletion to undo!")
            completion(false)
            return
        }
        
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {
            debugPrint("ERROR: Could not get appDelegate reference! (For goal deletion recovery)")
            completion(false)
            return
        }
        
        // Make a new goal and put the recovered goal data into it
        var recoveredGoal = Goal(context: managedContext)
        goalToRecover.setGoalEntity(goalToSet: &recoveredGoal)
        self.lastDeletedGoal = nil
    
        // Pass this object into persistent storage
        do {
            try managedContext.save()
            print("Successfully undone deleted goal!")
            
            UIView.animate(withDuration: 0.3) {
                self.undoRemoveView.isHidden = true
            }
            completion(true)
        }catch {
            debugPrint("Could not undo: \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func addSwipe() {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(animateViewDown))
        swipeGesture.direction = .down
        undoRemoveView.addGestureRecognizer(swipeGesture)
    }
    
    @objc func animateViewDown() {
        lastDeletedGoal = nil
        self.undoRemoveView.isHidden = true
    }
}

extension GoalsViewController {
    
    func setProgressForGoal(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let chosenGoal = goals[indexPath.row]
        if chosenGoal.goalProgress < chosenGoal.goalCompletionValue {
            chosenGoal.goalProgress += 1
        }else {
            return
        }
        
        do {
            try managedContext.save()
            print("Successfully set progress.")
        }catch {
            debugPrint("Could not save \(error.localizedDescription)")
        }
    }
    
    func fetch(completion: CompletionHandler) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let fetchRequest = NSFetchRequest<Goal>(entityName: "Goal")
        do {
            goals = try managedContext.fetch(fetchRequest)
            print("Successfully fetched data.")
            completion(true)
        }catch {
            debugPrint("Could not fetch \(error.localizedDescription)")
            completion(false)
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATransitionType(_ input: CATransitionType) -> String {
	return input.rawValue
}
