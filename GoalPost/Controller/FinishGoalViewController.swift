//
//  FinishGoalViewController.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import CoreData

class FinishGoalViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var createGoalButton: UIButton!
    @IBOutlet weak var pointsTextField: UITextField! { didSet { pointsTextField.delegate = self } }
    
    // MARK: - Private variables
    private var goalDescription: String!
    private var goalType: GoalType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createGoalButton.bindToKeyboard()
    }

    
    func initData(description: String, type: GoalType) {
        goalDescription = description
        goalType = type
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        pointsTextField.resignFirstResponder()
        dismissDetail(withTransition: convertFromCATransitionType(CATransitionType.fade))
    }
    
    @IBAction func createGoalButtonPressed(_ sender: UIButton) {
        if pointsTextField.text != "" && pointsTextField.text != nil {
            save { success in
                if success {
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    

    func save(completion: CompletionHandler) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let goal = Goal(context: managedContext)
        goal.goalDescription = goalDescription
        goal.goalType = goalType.rawValue
        goal.goalCompletionValue = Int32(pointsTextField.text ?? "0")!
        goal.goalProgress = Int32(0)
        
        do {
            try managedContext.save()
            print("Successfully saved in CoreData")
            completion(true)
        }catch {
            debugPrint("Could not save \(error.localizedDescription)")
            completion(false)
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromCATransitionType(_ input: CATransitionType) -> String {
	return input.rawValue
}
