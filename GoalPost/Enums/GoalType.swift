//
//  GoalType.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation

enum GoalType: String {
    case shortTerm = "Short Term"
    case longTerm =  "Long Term"
}
