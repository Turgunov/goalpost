//
//  GoalCell.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class GoalCell: UITableViewCell {

    @IBOutlet weak var goalDescriptionLabel: UILabel!
    @IBOutlet weak var goalTypeLabel: UILabel!
    @IBOutlet weak var goalProgressLabel: UILabel!
    
    @IBOutlet weak var completionView: UIView!

    
    func configureCell(goal: Goal) {
        goalDescriptionLabel.text = goal.goalDescription
        goalTypeLabel.text = goal.goalType
        goalProgressLabel.text = String(describing: goal.goalProgress)
        
        if goal.goalProgress == goal.goalCompletionValue {
            completionView.isHidden = false
        }else {
            completionView.isHidden = true
        }
    }
}
