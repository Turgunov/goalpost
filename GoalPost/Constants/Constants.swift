//
//  Constants.swift
//  GoalPost
//
//  Created by Olimjon Turgunov on 25.06.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

typealias CompletionHandler = (_ success: Bool)->()

import Foundation

struct Cells {
    static let goalCell = "goalCell"
}

struct StoryboardViewControllerIDs {
    static let createGoalVC = "CreateGoalViewController"
    static let finishGoalVC = "FinishGoalViewController"
    
}
